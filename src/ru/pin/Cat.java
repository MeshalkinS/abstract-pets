package ru.pin;

/**
 * Created by admin on 03.10.2015.
 */
public class Cat extends Pets {

    @Override
    String move() {
        return("Крадётся");
    }

    @Override
    String talk() {
        return ("Мяу - Мяу");
    }

    @Override
    String nameAnimal() {
        return ("Кот");
    }

    @Override
    String outInfoAnimal() {
        return (nameAnimal() + " говорит " + talk() + " и " + move() + " в закат.");
    }
}
