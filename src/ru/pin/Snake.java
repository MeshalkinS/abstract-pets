package ru.pin;

/**
 * Created by admin on 27.10.2015.
 */
public class Snake extends Pets {
    @Override
    String move() {
        return ("Ползёт");
    }

    @Override
    String talk() {
        return ("Шш-шш-шс");
    }

    @Override
    String nameAnimal() {
        return ("Змея");
    }

    @Override
    String outInfoAnimal() {
        return (nameAnimal() + " говорит " + talk() + " и " + move() + " в закат.");
    }
}
