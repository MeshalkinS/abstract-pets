package ru.pin;

/**
 * Created by admin on 08.11.2015.
 */
public class Frog extends Pets {
    @Override
    String move() {
        return ("Прыгает");
    }

    @Override
    String talk() {
        return ("Ква-ква-ква");
    }

    @Override
    String nameAnimal() {
        return ("Лягушка");
    }

    @Override
    String outInfoAnimal() {
        return (nameAnimal() + " говорит " + talk() + " и " + move() + " в закат.");
    }
}
