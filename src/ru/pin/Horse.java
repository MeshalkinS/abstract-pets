package ru.pin;

/**
 * Created by admin on 03.10.2015.
 */
public class Horse extends Pets {
    @Override
     String move() {
       return ("Скачет");
    }

    @Override
    String talk() {
        return ("И-го-го");
    }

    @Override
    String nameAnimal() {
        return ("Лошадка");
    }

    @Override
    String outInfoAnimal() {
        return (nameAnimal() + " говорит " + talk() + " и " + move() + " в закат.");
    }
}
